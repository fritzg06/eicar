#requires -version 2
<#
.SYNOPSIS
  This is a powershell script that will download eicar.com from eicar website.
.DESCRIPTION
  This script uses powershell System.Net.WebClient to download eicar.com test malware file to a temporary directory.
.INPUTS
  None
.OUTPUTS
  None
.NOTES
  Version:        0.01
  Author:         Fritz Reyes
  Creation Date:  2018/05/24
  Purpose/Change: Initial script development
#>

#----------------------------------------------------------[Declarations]----------------------------------------------------------


#---------------------------------------------------------[Initializations]--------------------------------------------------------
$i = 1
[int]$NumberToDeploy = 1

#-----------------------------------------------------------[Functions]------------------------------------------------------------
function tempDir {
$tempDir = 'C:\_temp_eicar'
if(!(Test-Path -Path $tempDir )){
    New-Item -ItemType directory -Path $tempDir
	}
}

function downloadEicarCom {
while ($i -le $NumberToDeploy) {
    Write-Host "Attempting to download eicar$i.com"
    $WebClient = New-Object System.Net.WebClient
    $WebClient.DownloadFile("http://www.eicar.org/download/eicar.com","C:\_temp_eicar\eicar$i.com")
    $i++
    }
}

#-----------------------------------------------------------[Execution]------------------------------------------------------------
tempDir
downloadEicarCom
Write-Host "`nScript completed." -ForegroundColor Yellow