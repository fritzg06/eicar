#requires -version 2
<#
.SYNOPSIS
  This is a powershell script that will attempt to create an eicar file from eicar text string from eicar site.
.DESCRIPTION
  This script uses powershell to attempt to recreate an eicar file with the text string.
.INPUTS
  None
.OUTPUTS
  None
.NOTES
  Version:        0.01
  Author:         Fritz Reyes
  Creation Date:  2018/11/30
  Purpose/Change: Initial script development
#>

#----------------------------------------------------------[Declarations]----------------------------------------------------------


#---------------------------------------------------------[Initializations]--------------------------------------------------------


#-----------------------------------------------------------[Functions]------------------------------------------------------------
function tempDir {
$tempDir = 'C:\_temp_eicar'
if(!(Test-Path -Path $tempDir )){
    New-Item -ItemType directory -Path $tempDir
	}
}

function eicarCreate {
$eicarWebRequest = invoke-webrequest http://2016.eicar.org/86-0-Intended-use.html
$eicarWebRequest.Content > C:\_temp_eicar\eicar.html
$file2 = Select-String -Pattern "7CC.*EICAR-STANDARD-ANTIVIRUS-TEST-FILE" -Path C:\_temp_eicar\eicar.html
$file2 > C:\_temp_eicar\eicar.exe

$string = get-content -path C:\_temp_eicar\eicar.exe
$string1 = $string.split(">")

$string2 = $string1.split("<")
$string2 > C:\_temp_eicar\eicar2.exe

$file3 = Select-String -Pattern "EICAR-STANDARD-ANTIVIRUS-TEST-FILE" -Path C:\_temp_eicar\eicar2.exe
$string3 = select-string -path C:\_temp_eicar\eicar2.exe -Pattern "EICAR-STANDARD-ANTIVIRUS-TEST-FILE" | select line | ft -HideTableHeaders
$string3 > C:\_temp_eicar\eicar3.exe

$string4 = get-content -path C:\_temp_eicar\eicar3.exe | where {$_ -ne ""}
$string4 > C:\_temp_eicar\eicar4.exe

$missingString = "X5O!P%@AP[4\P"

$string5 = $missingString + $string4.trim()
$string5 | Out-File -Encoding ascii -filepath C:\_temp_eicar\eicar5.exe

$fileFinal = "C:\_temp_eicar\test_eicar.exe"
[System.IO.File]::WriteAllText($fileFinal,$string5,[System.Text.Encoding]::ASCII)

Remove-Item -Force C:\_temp_eicar\eicar.html
Remove-Item -Force C:\_temp_eicar\eicar.exe
Remove-Item -Force C:\_temp_eicar\eicar2.exe
Remove-Item -Force C:\_temp_eicar\eicar3.exe
Remove-Item -Force C:\_temp_eicar\eicar4.exe
Remove-Item -Force C:\_temp_eicar\eicar5.exe
}

#-----------------------------------------------------------[Execution]------------------------------------------------------------
tempDir
eicarCreate
Write-Host "`nScript completed." -ForegroundColor Yellow